/*
 * Welcome to your app's main JavaScript file!
 *
 * We recommend including the built version of this JavaScript file
 * (and its CSS file) in your base layout (base.html.twig).
 */

// any CSS you import will output into a single css file (app.css in this case)
import './styles/app.css';

const $ = require('jquery');
require('bootstrap');

require('bootstrap/dist/css/bootstrap.css');
require('@fortawesome/fontawesome-free/css/all.min.css');

// jQuery section
$(document).ready(function() {

	// Init bootstrap events
	$('[data-toggle="popover"]').popover();
	$('[data-toggle="tooltip"]').tooltip()

	/* Sirvoy calendar */
	$('body').on('click','.sirvoy-calendar-nav',function() {
		var _container=$(this).closest('.sirvoy-calendar');
		_container.addClass('calendar-ajax-busy');
			
		var jqxhr = $.post('/sirvoy-calendar-ajax',{ date: $(this).attr('data-date') },function(response) {
			_container.replaceWith(response);
			$('[data-toggle="tooltip"]').tooltip();
 			$('[data-toggle="popover"]').popover();
		});

		jqxhr.always(function() {
    		_container.removeClass('calendar-ajax-busy');
    	});
	});
	
	/* Checkbox bulk operations */
	$('input.bulk-check-all').on('change',function() {
		var state=$(this).is(':checked');
		var checkboxes=$(this).closest('table').find('td.bulk-operation-checkbox input[type="checkbox"]');
		checkboxes.prop('checked',state);
	});

	/* Task ajax modal */
	$("#taskActionsModal").on('show.bs.modal',function(event) {
		var trigger = $(event.relatedTarget) 
		var modal = $(this)
		var action=trigger.attr('data-action');
		var loadingMarkup='<span class="spinner-border spinner-border-sm"></span>';

		// Set modal text depends on action
		if(action=='remove') {
			modal.find('.modal-title').text('Task removing');
			modal.find('.modal-body').text('Are you sure you want to delete the task '+trigger.attr('data-task-title')+'?')
		}

		// Execute action if confirm trigger executed
		var confirmTrigger=modal.find('.task-modal-confirm-action');
		confirmTrigger.on('click',function() {
			confirmTrigger.attr('data-original-text',confirmTrigger.text()).attr('disabled','disabled').html(loadingMarkup);
			
			var jqxhr = $.post('/task-ajax',{action: action, task: trigger.attr('data-task-id')},function(response) {
				if(response.status!='success') {
					modal.find('.modal-body').html('<span class="text-danger"><i class="fas fa-exclamation-triangle"></i> '+response.message+'</span>');
					confirmTrigger.remove();
				} else {
					modal.find('.modal-body').text('Reloading...');
					confirmTrigger.remove();
					location.reload();
				}
			});

			jqxhr.always(function() {
				confirmTrigger.text(confirmTrigger.attr('data-original-text')).removeAttr('data-original-text').removeAttr('disabled');
	    	});
		});
	});
});
