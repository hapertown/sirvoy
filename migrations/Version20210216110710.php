<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210216110710 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task CHANGE uid uid INT DEFAULT NULL, CHANGE priority priority INT DEFAULT NULL, CHANGE status status INT DEFAULT NULL, CHANGE tags_id tags_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE task ADD CONSTRAINT FK_527EDB258D7B4FB4 FOREIGN KEY (tags_id) REFERENCES task_tag (id)');
        $this->addSql('CREATE INDEX IDX_527EDB258D7B4FB4 ON task (tags_id)');
        $this->addSql('ALTER TABLE user CHANGE roles roles JSON NOT NULL');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('ALTER TABLE task DROP FOREIGN KEY FK_527EDB258D7B4FB4');
        $this->addSql('DROP INDEX IDX_527EDB258D7B4FB4 ON task');
        $this->addSql('ALTER TABLE task CHANGE uid uid INT DEFAULT NULL, CHANGE status status INT DEFAULT NULL, CHANGE priority priority INT DEFAULT NULL, CHANGE tags_id tags_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE user CHANGE roles roles LONGTEXT CHARACTER SET utf8mb4 NOT NULL COLLATE `utf8mb4_bin`');
    }
}
