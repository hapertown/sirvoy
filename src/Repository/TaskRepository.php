<?php

namespace App\Repository;

use App\Entity\Task;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;
use App\Entity\User;

/**
 * @method Task|null find($id, $lockMode = null, $lockVersion = null)
 * @method Task|null findOneBy(array $criteria, array $orderBy = null)
 * @method Task[]    findAll()
 * @method Task[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class TaskRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Task::class);
    }
    
    /**
    * @return Task[] Returns an array of TaskStatus objects
    */
    public function findByCalendarMonth(\DateTime $date, User $user) {
        
        return $this->createQueryBuilder('t')
        ->where('t.date >= :firstDay')
        ->setParameter('firstDay', $date->format('Y-m-1'))
        ->andWhere('t.date <= :lastDay')
        ->setParameter('lastDay',$date->format('Y-m-t'))
        ->andWhere('t.uid = :uid')
        ->setParameter('uid',$user)
        ->getQuery()
        ->getResult();
    }
    
    /**
     * @return Task[] Returns an array of TaskStatus objects
     */
    public function findExpired(User $user) {
        return $this->createQueryBuilder('t')
        ->leftJoin('t.status', 'task_status')
        ->where('task_status.machine_name <> :machine_name')
        ->setParameter('machine_name', 'completed')
        ->andWhere('t.deadline < :deadline')
        ->setParameter('deadline',new \DateTime())
        ->andWhere('t.uid = :uid')
        ->setParameter('uid',$user)
        ->getQuery()
        ->getResult();
    }
}
