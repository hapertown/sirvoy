<?php 

namespace App\Sirvoy\Calendar\Generator;

use App\Entity\Task;

/**
   * CalendarDay
   * 
   * 
   * @package    SirvoyCalendar
   * @subpackage Controller
   */
class CalendarDay {
    
    /** @var DateTime Current day date */
    private $date;
    
    /** @var array Collection of Task objects */
    private $tasks;
    
    /**
     * Constructor
     *
     * @param DateTime|null  Base date of day
     * @return nothing
     */ 
    public function __construct(\DateTime $date) {
        $this->date=$date;
        $this->tasks=[];
    }
    
     /**
     * Getter for date property
     *
     * @return DateTime $date property
     */ 
    public function getDate() {
        return $this->date;
    }
    
     /**
     * Get day number of current date
     *
     * @return string  Day number
     */ 
    public function getNumber() {
        return $this->date->format('d');
    }

    /**
     * Get all tasks in Day
     *
     * @return array  Task objects collection
     */ 
    public function getTasks() {
        return $this->tasks;
    }
    
     /**
     * Check if date is current day (today)
     *
     * @return bool
     */ 
    public function isToday() {
        return (bool)($this->date->format('Y-m-d')===(new \DateTime())->format('Y-m-d'));
    }
    
     /**
     * Check if date is Sunday
     *
     * @return bool
     */ 
    public function isSunday() {
        return (bool)($this->date->format('w')==0);
    }
    
     /**
     * Add new task to Day
     *
     * @param $task  Task object 
     * @return nothing
     */ 
    public function addTask(Task $task) {
        $this->tasks[]=$task;
    }
}