<?php 

namespace App\Sirvoy\Calendar\Generator;

use App\Entity\Task;

/**
   * CalendarMaker
   * 
   * 
   * @package    SirvoyCalendar
   * @subpackage Controller
   */
class CalendarMaker {
    
    /** @var DateTime Base date of calendar */
    private $date;
    
    /** @var DateTime First day of calendar */
    private $firstDay;
    
    /** @var DateTime Last day of calendar */
    private $lastDay;
    
    /** @var array CalendarDay collection - all days in month */
    private $calendarDays;
    
    /** @var array Short names of each day in week */
    private $calendarDaysNames;
    
    /** @var array Task collection in month */
    private $tasks;
       
    /**
     * Getter for fistDay property
     *
     * @return DateTime $firstDay property
     */ 
    public function getFirstDay() {
        return $this->firstDay;
    }
    
    /**
     * Getter for lastDay property
     *
     * @return DateTime $lastDay property
     */ 
    public function getLastDay() {
        return $this->lastDay;
    }
    
    /**
     * Getter for date property
     *
     * @return DateTime $date property
     */ 
    public function getDate() {
        return $this->date;
    }
    
    /**
     * Constructor
     *
     * @param DateTime|null  Base date of calendar
     * @return nothing
     */ 
    public function __construct(\DateTime $date = null) {
        $this->date=($date ?? new \DateTime());
        $this->firstDay=new \DateTime($this->date->format('Y-m-1'));
        $this->lastDay=new \DateTime($this->date->format('Y-m-t'));
        $this->calendarDays=self::buildCalendarDays();
        
        $this->calendarDaysNames=['Mon','Tue','Wed','Thu','Fri','Sat','Sun'];
    }
    
    /**
     * Build calendar days in current month.
     *
     * @return array - Calendar structure required for twig render
     */
    private function buildCalendarDays(): array
    {
        // Init array
        $days=[];
        
        // Get count of days in month
        $interval=$this->firstDay->diff($this->lastDay);

        // Count days loop
        for($i=0;$i<=(int)($interval->days);$i++) {
            // Create date from first day
            $calendarDate=new \DateTime($this->firstDay->format('Y-m-d'));

            // Increase date
            $calendarDate->modify('+'.$i.' day');

            // Set CalendarDate object to array
            $days[$calendarDate->getTimestamp()]=new CalendarDay($calendarDate);
        }
        
        // Return list
        return $days;
    }
    
    /**
     * Build calendar structure prepare to render
     *
     * @return array - list of CalendarDate objects
     */
    public function renderCalendarStructure() {
        
        // Checking first day before
        $calendarBeforeDays=(int)$this->firstDay->format('w');
        $calendarBeforeDays=($calendarBeforeDays==0 ? 6 : $calendarBeforeDays-1);
        
        // Refill missing days before month
        for($i=1;$i<=$calendarBeforeDays;$i++) {
            $beforeDay=new \DateTime($this->firstDay->format('Y-m-d'));
            $beforeDay->modify('-'.$i.' day');
            $this->calendarDays[$beforeDay->getTimestamp()]=new CalendarDay($beforeDay);
        }
        
        // Checking last day
        $calendarAfterDays=(int)($this->lastDay->format('w'));
        $calendarAfterDays=($calendarAfterDays==0 ? 0 : 7-$calendarAfterDays);
        
        // Refill missing days after month
        for($i=1;$i<=$calendarAfterDays;$i++) {
            $afterDay=new \DateTime($this->lastDay->format('Y-m-d'));
            $afterDay->modify('+'.$i.' day');
            $this->calendarDays[$afterDay->getTimestamp()]=new CalendarDay($afterDay);
        }
        
        // Sort calendar days items
        ksort($this->calendarDays);
        
        // Prepare structure
        $structure=[
            'weeks'=>array_chunk($this->calendarDays,7),
            'dayNames'=>$this->calendarDaysNames,
            'baseDate'=>$this->date,
            'nextDate'=>(new \DateTime($this->date->format('Y-m-1')))->modify('+1 month'),
            'previousDate'=>(new \DateTime($this->date->format('Y-m-1')))->modify('-1 month'),
        ];
        
        // Export structure
        return $structure;
    }
    
     /**
     * Add new task to calendar
     *
     * @param object Task object
     * @return nothing
     */
    public function addTask(Task $task) {
        $createdTime=$task->getDate();
        $timestamp=$createdTime->setTime(0,0,0,0)->getTimestamp();
        
        if(array_key_exists($timestamp,$this->calendarDays)) {
            $this->calendarDays[$timestamp]->addTask($task);
        }
    }
    
    /**
     * Add new tasks to calendar
     *
     * @param array Task objects collection
     * @return nothing
     */
    public function addTasks(array $tasks) {
        foreach($tasks as $task) {
            self::addTask($task);
        }
    }
}