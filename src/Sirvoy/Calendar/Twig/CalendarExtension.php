<?php

namespace App\Sirvoy\Calendar\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use Twig\TwigFunction;
use Twig\Environment;
use App\Sirvoy\Calendar\Generator\CalendarMaker;

class CalendarExtension extends AbstractExtension
{
    /*public function getFilters(): array
    {
        return [
            // If your filter generates SAFE HTML, you should add a third
            // parameter: ['is_safe' => ['html']]
            // Reference: https://twig.symfony.com/doc/2.x/advanced.html#automatic-escaping
            new TwigFilter('filter_name', [$this, 'doSomething']),
        ];
    }*/

    public function getFunctions(): array
    {
        return [
            new TwigFunction('sirvoy_calendar_render', [$this, 'render'],['is_safe' => ['html'], 'needs_environment' => true]),
        ];
    }

    public function render(Environment $env, array $structure)
    {
        //print \dirname(__DIR__); die();
        return $env->render('calendar/calendar.html.twig',['calendar'=>$structure]);
        //return $this->render('whatever.html.twig');
        //return $env->render('calendar.html.twig');
    }
}
