<?php

namespace App\Sirvoy\Calendar\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;
use App\Repository\TaskRepository;
use App\Entity\Task;
use App\Sirvoy\Calendar\Generator\CalendarMaker;

class CalendarController extends AbstractController
{
	/**
	* @Route("/sirvoy-calendar-ajax", name="app_sirvoy_calendar_ajax")
	*/
    public function index(TaskRepository $taskRepository, Request $request, Security $security)
    {
        // Check if request is Ajax
        if($request->isXmlHttpRequest()) {

            // Set default begin date
            $date=new \DateTime($request->get('date',null));

            // Create CalendarMaker object
            $calendar=new CalendarMaker($date);

            // Load tasks by current user
            $tasks=$taskRepository->findByCalendarMonth($calendar->getDate(),$security->getToken()->getUser());

            // Add tasks to calendar
            $calendar->addTasks($tasks);
           
            // Render view
            return $this->render('calendar/calendar.html.twig',['calendar'=>$calendar->renderCalendarStructure()]);
        }
    }
}