<?php

namespace App\Controller;

use App\Entity\Task;
use App\Form\TaskBulkOperationsType;
use App\Form\TaskFiltersType;
use App\Form\TaskFormType;
use App\Repository\TaskRepository;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\TaskStatus;
use App\Entity\TaskPriority;
use joshtronic\LoremIpsum;
use App\Entity\User;
use Symfony\Component\Security\Core\Security;

class TaskController extends AbstractController
{
    /**
    * @Route("/panel/task/show/{id}", requirements={"id"="\d+"}, name="app_user_show_task")
    */
    public function show(Task $task, Security $security, Request $request): Response
    {
        // If object not found, return 404
        if(!$task) {
            throw $this->createNotFoundException('The Task does not exist');
        }

        // Check permissions
        if($task->getUid()!=$security->getToken()->getUser()) {
            throw $this->createAccessDeniedException('Access denied for this task!');
        }

        return $this->render('panel/task/show.html.twig',['task' => $task]);
    }

	/**
	* @Route("/panel/task/add", name="app_user_add_task")
	*/
    public function add(Request $request): Response
    {
        // Load current logged user
        $user = $this->container->get('security.token_storage')->getToken()->getUser();
        
    	// Fresh task instance
        $task = new Task();

        // Load form by instance
        $form = $this->createForm(TaskFormType::class, $task);

        // Handle form
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
			
            // Set logged user UID
			$task->setUid($user);
    
			// Save object
			$em->persist($task);
			$em->flush();
    
			// Set notice
            $this->addFlash('notice','Task '.$task->getTitle().' has been created!');
            
            // Redirect to list
            return $this->redirectToRoute('app_user_tasks_manage');
        }

        return $this->render('panel/task/add.html.twig',['form' => $form->createView()]);
    }

    /**
    * @Route("/panel/task/edit/{id}", requirements={"id"="\d+"}, name="app_user_edit_task")
    */
    public function edit($id, Security $security,   Request $request): Response
    {
        // Load object by ID
        $task = $this->getDoctrine()
            ->getRepository(Task::class)
            ->find($id);

        // If object not found, return 4040
        if(!$task) {
            throw $this->createNotFoundException('The Task does not exist');
        }

        // Check permissions
        if($task->getUid()!=$security->getToken()->getUser()) {
            throw $this->createAccessDeniedException('Access denied for this task!');
        }
    
        // Create form from Task
        $form = $this->createForm(TaskFormType::class, $task);
    
        // Handle form
        $form->handleRequest($request);
        if ($form->isSubmitted() && $form->isValid()) {
            // Save changes
            $em = $this->getDoctrine()->getManager();
            $em->persist($task);
            $em->flush();

            // Set notice
            $this->addFlash('notice','Task has been updated!');
            
            // Redirect to list
            return $this->redirectToRoute('app_user_tasks_manage');
        }

        return $this->render('panel/task/edit.html.twig',['form' => $form->createView()]);
    }

    /**
	* @Route("/panel/task/manage", name="app_user_tasks_manage")
	*/
    public function manage(Security $security, PaginatorInterface $paginator, Request $request): Response
    {
        // Load current logged user
        $user = $security->getToken()->getUser();
        
        // Build filters form
        $filtersForm=$this->createForm(TaskFiltersType::class,null,['method'=>'GET']);
        $filtersForm->handleRequest($request);
       
        // Build Tasks query
        $repository = $this->getDoctrine()->getRepository('App\Entity\Task');
        $query = $repository->createQueryBuilder('t')
        ->where('t.uid = :uid')
        ->setParameter('uid', $user->getId());

        // Execute Filters
        $query=self::taskManageFiltersExecute($filtersForm,$query);

        // Build paginator
        $paginator = $paginator->paginate($query,$request->query->getInt('page', 1),30);
        
        // Extract tasks
        $tasks=$query->getResult();
    
        // Create bulk operations form
        $bulkOperationsForm=$this->createForm(TaskBulkOperationsType::class, null, ['entity_manager'=>$this->getDoctrine()->getManager()]);
        $bulkOperationsForm->add('tasks', EntityType::class, [
            'class' => Task::class,
            'choices' => $tasks,
            'choice_label'=>function ($choice, $key, $value) { return false; },
            'expanded'=>TRUE,
            'multiple'=>TRUE,
        ]);
        
         
        // Execute Bulk Operation action
        $bulkOperationsForm->handleRequest($request);
        if($bulkOperationsForm->isSubmitted() && $bulkOperationsForm->isValid()) {
            // Load selected tasks from form
            $selectedTasks=$bulkOperationsForm->get('tasks')->getData();
            
            // Load action to execute
            $selectedAction=$bulkOperationsForm->get('action')->getData();
            
            // If any task selected, execute action
            if($selectedTasks->count()) {
                // Parse action parameters
                $selectedAction=explode('__',$selectedAction);
                
                // Execute action
                self::taskBulkOperationExecute($selectedAction[1], $selectedAction[2] ?? NULL, $selectedTasks);
                
                // Redirect to list
                return $this->redirectToRoute('app_user_tasks_manage');
            }
        }
        
        // Render view
        return $this->render('panel/task/manage.html.twig',
        [
            'tasks'=>$tasks,
            'pagination'=>$paginator,
            'bulkOperationsForm'=>$bulkOperationsForm->createView(),
            'filtersForm'=>$filtersForm->createView()
        ]);
    }

    /**
    * @Route("/task-ajax", name="app_user_tasks_ajax")
    */
    public function ajax(Request $request, TaskRepository $taskRepository, Security $security): Response
    {
        $json=[
            'status'=>'success',
            'message'=>''
        ];

        if($request->isXmlHttpRequest()) {
            $action=$request->get('action',null);
            $taskId=$request->get('task',null);
            $task=$taskRepository->find($taskId);

            // Check if task exists
            if(!$task) {
                $json['status']='error';
                $json['message']='Task not found';
                return new JsonResponse($json);
            }

            // Check permissions
            if($security->getToken()->getUser()!=$task->getUid()) {
                $json['status']='error';
                $json['message']='You dont have permission to execute action of this Task';   
                return new JsonResponse($json);
            }

            // Execute action
            switch($action) {
                // Remove task
                case 'remove':
                    $em = $this->getDoctrine()->getManager();
                    $em->remove($task);
                    $em->flush();

                    $this->addFlash('notice','Task has been removed!');
                    break;
            }
        }
        
        return new JsonResponse($json);
    }
    
    /**
     * @Route("/panel/task/list/{type}", requirements={"type"="today|expired"}, name="app_user_tasks_list")
     */
    public function list(string $type, TaskRepository $taskRepository, Security $security): Response
    { 
        $tasks=[];
        
        switch($type) {
            
            case 'today':
                $tasks=$taskRepository->findBy(['date'=>new \DateTime()]);
                break;
                
            case 'expired':
                $tasks=$taskRepository->findExpired($security->getToken()->getUser());
                break;
                
            default:
                break;
        }
        
        return $this->render('panel/task/list.html.twig',['tasks'=>$tasks]);
    }
    
    /**
       * 
       * Execute bulk operations for tasks
       *
       * @param string $property  Task property to change (status, priority)
       * @param string $value  New value to set in task based on machine name (eg. Hight priority). Empty if property is 'remove'
       * @param array $tasks  Task objects collection
       * @return nothing
    */
    private function taskBulkOperationExecute(string $property, $value, $tasks)
    {
        
        // Entity Manager
        $em = $this->getDoctrine()->getManager();
        
        // Preload statuses
        $statuses=$this->getDoctrine()->getRepository(TaskStatus::class)->findAll();
        $statusesMap=[];
        foreach($statuses as $status) {
            $statusesMap[$status->getMachineName()]=$status;
        }
        
        // Preload values
        $priorities=$this->getDoctrine()->getRepository(TaskPriority::class)->findAll();
        $prioritiesMap=[];
        foreach($priorities as $priority) {
            $prioritiesMap[$priority->getMachineName()]=$priority;
        }
        
        // Loop modified tasks
        foreach($tasks as $task) {
            
            switch($property) {
                // Set new status
                case 'status':
                    $task->setStatus($statusesMap[$value]);
                    $em->persist($task);
                break;
                
                // Set new priority
                case 'priority':
                    $task->setPriority($prioritiesMap[$value]);
                    $em->persist($task);
                    break;
                    
                // Remove item
                case 'remove':
                    $em->remove($task);
                    break;
                
                default:
                break;
            }
            
            // Execute changes
            $em->flush();
        }

        if(in_array($property,['status','priority'])) {
            $this->addFlash('notice', 'Selected tasks '.$property.' has been changed to '.$statusesMap[$value]->getName().'!');
        }
        if($property=='remove') {
            $this->addFlash('notice', 'Selected tasks has been removed!');
        }
    }

    /**
       * 
       * Execute manage tasks filters
       *
       * @param object $filtersForm  Form executed as 'Filters form' 
       * @param string $query  Base query search tasks
       * @return object $query Modified query
    */
    private function taskManageFiltersExecute($filtersForm,$query)
    {
        // Filter values
        foreach(['priority','status'] as $property) {
            $filterValue=$filtersForm->get($property)->getData();
            if(!$filterValue) { continue; }
            
            $query->andWhere('t.'.$property.' = :'.$property)
            ->setParameter($property,$filterValue->getId());
        }
        
        // Filter title values
        if($filtersForm->get('title')->getData()) {
            $query->andWhere('t.title LIKE :title')
            ->setParameter('title','%'.$filtersForm->get('title')->getData().'%');
        }

        // Return whole query
        return $query->getQuery();
    }
}