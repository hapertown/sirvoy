<?php

namespace App\Controller;

use Symfony\Component\HttpFoundation\Response;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use App\Entity\Task;
use App\Sirvoy\Calendar\Generator\CalendarMaker;
use App\Repository\TaskRepository;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Security;

class DashboardController extends AbstractController
{
	/**
	* @Route("/panel/dashboard", name="app_user_dashboard")
	*/
    public function index(Security $security)
    {
        // Load all tasks
    	$tasks = $this->getDoctrine()
            ->getRepository(Task::class)
            ->findBy(['uid'=>$security->getToken()->getUser()]);

        // Statistics base array
        $taskStatistics=[
        	'all'=>['title'=>'All tasks','class'=>'light','count'=>count($tasks)],
        	'new'=>['title'=>'New tasks','class'=>'primary','count'=>0],
        	'completed'=>['title'=>'Completed tasks','class'=>'success','count'=>0],
        	'in_progress'=>['title'=>'In progress tasks','class'=>'warning','count'=>0],
        	'expired'=>['title'=>'Expired tasks','class'=>'danger','count'=>0],
        ];

        // Chart data
        $chartData=[
            'allByMonth'=>array_fill(1,12,0)
        ];

        // Loop statistics by status machine name
        foreach($tasks as $task) {
            $taskStatistics[$task->getStatus()->getMachineName()]['count']++;
    		$taskStatistics['expired']['count']+=(int)($task->isExpired());

            // Check if task is current year
            if($task->getDate()->format('Y')==(new \DateTime())->format('Y')) {
                $chartData['allByMonth'][(int)($task->getDate()->format('m'))]++;   
            }
        }

        // Render view
        return $this->render('panel/dashboard/index.html.twig',['statistics'=>$taskStatistics,'chartData'=> $chartData]);
    }
    
    /**
     * @Route("/panel/calendar", name="app_user_calendar")
     */
    public function calendar(TaskRepository $taskRepository, Security $security): Response
    {  
        // Get CalendarMaker object 
        $calendar=new CalendarMaker();

        // Load all tasks by current user
        $tasks=$taskRepository->findByCalendarMonth($calendar->getDate(),$security->getToken()->getUser());

        // Add tasks to calendar
        $calendar->addTasks($tasks);
        
        // Render view
        return $this->render('panel/dashboard/calendar.html.twig',['calendar'=>$calendar->renderCalendarStructure()]);
    }
}