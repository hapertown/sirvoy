<?php
namespace App\DataFixtures;
use App\Entity\User;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Security\Core\Encoder\UserPasswordEncoderInterface;
/**
 * Class UserFixtures
 * @package App\DataFixtures
 */
class UserFixtures extends Fixture {
  /**
   * @var UserPasswordEncoderInterface
   */
  private $encoder;
  /**
   * @var EntityManager
   */
  private $entityManager;
  /**
   * UserFixtures constructor.
   * @param UserPasswordEncoderInterface $encoder Password encoder instance
   */
  public function __construct(UserPasswordEncoderInterface $encoder, EntityManagerInterface $entityManager) {
    $this->encoder = $encoder;
    $this->entityManager = $entityManager;
  }
  /**
   * @param ObjectManager $manager Object manager instance
   *
   * @return void
   */
  public function load(ObjectManager $manager) : void {
        // First user
        $user = new User();
        $user->setName('matt');

        $password = $this->encoder->encodePassword($user, 'matt');
        $user->setPassword($password);

        $manager->persist($user);
        
        // Second user
        $user = new User();
        $user->setName('johan');
        
        $password = $this->encoder->encodePassword($user, 'johan');
        $user->setPassword($password);
        
        $manager->persist($user);
        
        $manager->flush();
  }
}