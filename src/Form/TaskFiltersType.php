<?php

namespace App\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\TaskPriority;
use App\Entity\TaskStatus;

class TaskFiltersType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('priority', EntityType::class, array(
                    'class' => TaskPriority::class,
                    'choice_label'=>'getTitle',
                    'expanded'=>FALSE,
                    'empty_data' => null ,
                    'required'=>FALSE
                ))
            ->add('status', EntityType::class, array(
                    'class' => TaskStatus::class,
                    'choice_label'=>'getName',
                    'expanded'=>FALSE,
                    'empty_data' =>null ,
                    'required'=>FALSE
                ))
            ->add('title', TextType::class,array('required'=>FALSE))
        ;
         $builder->add('filter', SubmitType::class, [
            'attr' => ['class' => 'btn-primary'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);
    }
}
