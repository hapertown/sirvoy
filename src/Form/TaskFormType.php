<?php

namespace App\Form;

use App\Entity\Task;
use App\Entity\TaskStatus;
use App\Entity\TaskPriority;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;

class TaskFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title')
            ->add('description')
            ->add('priority',EntityType::class,[
                'class' => TaskPriority::class,
                'choice_label'=>'getTitle',
                'expanded'=>FALSE
            ])
            ->add('status',EntityType::class,[
                'class'=>TaskStatus::class,
                'choice_label'=>'getName',
                'expanded'=>TRUE
            ])
            ->add('date',DateType::class, [
                'widget'=>'single_text',
                'attr'=> ['min' => ( new \DateTime() )->format('Y-m-d') ],
                'label'=>'Execution date'
            ])
            ->add('deadline',DateType::class, [
                'widget'=>'single_text',
                'attr'   => ['min' => ( new \DateTime() )->format('Y-m-d') ]
            ]);

        $builder->add('save', SubmitType::class, [
            'attr' => ['class' => 'btn-primary'],
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Task::class,
        ]);
    }
}
