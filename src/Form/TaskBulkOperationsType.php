<?php

namespace App\Form;
use App\Entity\Task;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\CollectionType;
use Symfony\Component\Form\ChoiceList\ChoiceList;
use App\Entity\TaskStatus;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use App\Entity\TaskPriority;

class TaskBulkOperationsType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $entityManager = $options['entity_manager'];
        
        $statuses=$entityManager->getRepository(TaskStatus::class)->findAll();
        $priorities=$entityManager->getRepository(TaskPriority::class)->findAll();
        
        $choicesMap=[
            'Delete'=>'action__remove',
            'Change status'=>[],
            'Change priority'=>[]
        ];
        
        foreach($statuses as $status) {
            $choicesMap['Change status'][$status->getName()]='action__status__'.$status->getMachineName();
        }
        foreach($priorities as $priority) {
            $choicesMap['Change priority'][$priority->getTitle()]='action__priority__'.$priority->getMachineName();
        }
        
        $builder->add('action', ChoiceType::class, [
            'choices' => $choicesMap,
        ]);
        
        $builder->add('save', SubmitType::class, [
            'attr' => ['class' => 'btn btn-primary'],
            'label' => 'Execute'
        ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            // Configure your form options here
        ]);

        $resolver->setRequired('entity_manager');
    }
}
