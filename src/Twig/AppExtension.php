<?php

namespace App\Twig;

use Twig\Extension\AbstractExtension;
use Twig\TwigFilter;
use App\Entity\TaskPriority;
use App\Entity\Task;
use App\Entity\TaskStatus;

class AppExtension extends AbstractExtension
{
    public function getFilters()
    {
        return [
            new TwigFilter('priority', [$this, 'formatPriority']),
            new TwigFilter('deadline', [$this, 'formatDeadline']),
            new TwigFilter('status', [$this, 'formatStatus']),
        ];
    }

    public function formatPriority(TaskPriority $priority, string $format='default')
    {
        $output='';

        switch($format) {
            case 'default':
                $output='<span class="badge text-white" style="background: '.$priority->getColor().'";>'.$priority->getTitle().'</span>';
                break;
            default:
                break;
        }

        return $output;
    }

    public function formatDeadline(Task $task)
    {
        $classes=['badge'];
        $icon='fa-clock';

        if($task->isExpired()) {
            $icon='fa-exclamation-circle';
            $classes[]='badge-danger';
        } else {
            $classes[]='badge-light';
        }

        return '<span class="'.implode(' ',$classes).'"><i class="fas '.$icon.'"></i> '.$task->getDeadline()->format('Y-m-d').'</span>';

        return $output;
    }

     public function formatStatus(TaskStatus $taskStatus)
    {
        $classes=['badge'];

        switch($taskStatus->getMachineName()) {
            case 'new':
                $classes[]='badge-primary';
                break;

            case 'in_progress':
                $classes[]='badge-warning';
                break;

            case 'completed':
                $classes[]='badge-success';
                break;
                
            case 'expired':
                $classes[]='badge-danger';
                break;
            
            default:
                break;
        }

        return '<span class="'.implode(' ',$classes).'">'.$taskStatus->getName().'</span>';
    }
}